FROM debian:stable-slim

# Install guide: https://web.archive.org/web/20220426074406/https://seanthegeek.net/1172/how-to-install-volatility-2-and-volatility-3-on-debian-ubuntu-or-kali-linux/

RUN apt-get update && apt-get install -y \
    build-essential \
    git \
    libdistorm3-dev \
    yara \
    libraw1394-11 \
    libcapstone-dev \
    capstone-tool \
    tzdata \
    python2 \
    python2.7-dev \
    libpython2-dev \
    python3 \
    python3-dev \
    libpython3-dev \
    python3-pip \
    python3-setuptools \
    python3-wheel \
&& rm -rf /var/lib/apt/lists/* \
&& apt-get clean

# Install pip2 and Volatility2

COPY . /tmp

RUN python2 /tmp/get-pip.py && python2 -m pip install -U setuptools \
    wheel \
    distorm3 \
    yara-python \
    yara \
    pycrypto \
    pillow \
    openpyxl \
    ujson \
    pytz \
    ipython \
    capstone \
&& ln -s /usr/local/lib/python2.7/dist-packages/usr/lib/libyara.so /usr/lib/libyara.so \
&& python2 -m pip install -U git+file:///tmp/volatility.git \

# Install Volatility3
&& python3 -m pip install -U distorm3 \
    yara-python>=3.8.0 \
    yara \
    pycrypto \
    pillow \
    openpyxl \
    ujson \
    pytz \
    ipython \
    capstone \
&& python3 -m pip install -U git+file:///tmp/volatility3.git \
&& rm -rf /tmp/* \

# Symlinks to fix the confusion of vol.py/vol (vol2/vol3)
&& ln -s $(which vol.py) /usr/bin/vol2 \
&& ln -s $(which vol) /usr/bin/vol3

ENTRYPOINT ["/bin/bash","-c"] 
