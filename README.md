# volatility-container

Volatility2 and Volatility3 containerized in a Debian-container.

## Usage

### Load from pre-built image
```bash
~$ podman/docker load -i image/volatility-container.tar
```

### Build from Containerfile

```bash
~$ podman/docker build -t volatility-container .
```

### Run
`./docker-run.sh`  
or  
 `./podman-run.sh`  
or  
   `podman/docker run -it --rm --replace --name volatility-container -v memdumps:/mnt/memdumps volatility-container bash`

```bash
root@ce8ac61212fc:/# vol2 -h
Volatility Foundation Volatility Framework 2.6.1
Usage: Volatility - A memory forensics analysis platform.

Options:
  -h, --help            list all available options and their default values.
                        Default values may be set in the configuration file
                        (/etc/volatilityrc)
                        ...

root@ce8ac61212fc:/# vol3 --help
Volatility 3 Framework 2.0.3
usage: volatility [-h] [-c CONFIG] [--parallelism [{processes,threads,off}]] [-e EXTEND] [-p PLUGIN_DIRS] [-s SYMBOL_DIRS] [-v] [-l LOG] [-o OUTPUT_DIR] [-q] [-r RENDERER] [-f FILE] [--write-config] [--save-config SAVE_CONFIG]
                  [--clear-cache] [--cache-path CACHE_PATH] [--offline] [--single-location SINGLE_LOCATION] [--stackers [STACKERS ...]] [--single-swap-locations [SINGLE_SWAP_LOCATIONS ...]]
                  plugin ...

```

## Thanks
https://www.volatilityfoundation.org/  
https://seanthegeek.net/1172/how-to-install-volatility-2-and-volatility-3-on-debian-ubuntu-or-kali-linux  
https://www.pypa.io/en/latest/
